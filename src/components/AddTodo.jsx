import React, { Component } from "react";
class AddTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputContent: "",
    };
  }
  handleChange = (e) =>{
    this.setState({
      inputContent: e.target.value
    })
  }
  handleSubmit = (e) =>{
    e.preventDefault()
    this.props.handleAddTodo(this.state.inputContent)
    this.setState({
      inputContent: ""
    })
  }
  render() {
    return (
      <div>
        <form type= "submit" onSubmit={this.handleSubmit}>
          <input
            className="itemTextValue"
            id="todoValue"
            placeholder="Create a new todo..."
            type="text"
            value = {this.state.inputContent}
            onChange={this.handleChange}
          />
        </form>
      </div>
    );
  }
}

export default AddTodo;
