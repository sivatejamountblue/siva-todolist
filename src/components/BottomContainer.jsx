import React, { Component } from "react";
const statusElements = [
  {
    text: "All",
    filterValue: "all"
  },
  {
    text: "Active",
    filterValue: "active"
  },
  {
    text: "Completed",
    filterValue: "completed"
  },
]



class Bottom extends Component {
  render() {
    
    
    return (
      <div className="status-container">
        <div className="sub-status-container">
            {statusElements.map((item)=>{
              return(
                <button className="statusContainer button" onClick={()=> this.props.handleSetFilter(item)} key= {item.text}>
                  {item.text}
                </button>
              )
            })}
            <button className="button" onClick={()=> this.props.handleClearCompleted()}>Clear Completed</button>
        </div>
      </div>
    );
  }
}

export default Bottom;
