import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";
import Bottom from "./BottomContainer";
import AddTodo from "./AddTodo";
class Input extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      activefilter: "all",
      theme: false,
    };
  }
  handleAddTodo = (content) => {
    let x = {
      id: uuidv4(),
      content: content,
      isCompleted: false,
    };
    this.setState({ todos: [x, ...this.state.todos] });
  };
  handleTaskToggle = (id) => {
    this.setState({
      todos: this.state.todos.map((each) => {
        if (each.id === id) {
          return {
            ...each,
            isCompleted: !each.isCompleted,
          };
        }
        return each;
      }),
    });
  };
  handleDelete = (todoId) => {
    this.setState({
      todos: this.state.todos.filter((each) => each.id !== todoId),
    });
  };
  handleClearCompleted = () => {
    let filterArray = this.state.todos.filter((each) => !each.isCompleted);
    this.setState({
      todos: filterArray,
    });
  };
  getfilteredtask = () => {
    const { activefilter } = this.state;
    if (activefilter === "all") {
      return this.state.todos;
    } else if (activefilter === "active") {
      return this.state.todos.filter((each) => !each.isCompleted);
    } else if (activefilter === "completed") {
      return this.state.todos.filter((each) => each.isCompleted);
    }
    return [];
  };
  handleSetFilter = (item) => {
    this.setState({
      activefilter: item.filterValue,
    });
  };

  render() {
    const todosCount = this.state.todos.filter(
      (each) => !each.isCompleted
    ).length;
    const displayStatus = this.getfilteredtask();
    console.log(displayStatus)
    return (
      <div className="section-container">
        <AddTodo todos={this.state.todos} handleAddTodo={this.handleAddTodo} />
        <ul className={this.props.theme?"ul-container-dark":"ul-container"}>
          {displayStatus.map((each) => {
            return (
              <li key={each.id} className={this.props.theme?"li-container-dark":"li-container"}>
                <input
                  type="checkbox"
                  name="task-completed"
                  id="input-check"
                  checked={each.isCompleted}
                  onChange={() => this.handleTaskToggle(each.id)}
                />
                <label
                  className={each.isCompleted ? "strike" : ""}
                  htmlFor="input-check"
                >
                  {each.content}
                </label>
                <img
                  onClick={() => this.handleDelete(each.id)}
                  src="/images/icon-cross.svg"
                  alt=""
                  className="delete-image"
                />
              </li>
            );
          })}
        </ul>
        <footer className={this.props.theme?"footer-container-dark":"footer-container"}>
          <span className={this.props.theme?"counterValue-dark":"counterValue"}> {todosCount} items left</span>
          <Bottom
            handleClearCompleted={this.handleClearCompleted}
            handleSetFilter={this.handleSetFilter}
            activefilter={this.activefilter}
            todos={this.state.todos}
          />
        </footer>
      </div>
    );
  }
}

export default Input;
