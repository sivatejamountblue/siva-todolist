import React, { Component } from "react";
import "./App.css";
import Header from "./components/Header";
import Input from "./components/Input";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: false,
    };
  }
  handleThemeChange = () =>{
    this.setState({
      theme: !this.state.theme
    })
    console.log(this.state)
  }
  render() {
    return (
      <div className="container">
        <main className={this.state.theme?"bg-container-dark":"bg-container"} id="bgContainer">
          <div className="main-bg-container">
            <Header theme = {this.state.theme} handleThemeChange = {this.handleThemeChange}/>
            <Input theme = {this.state.theme} />
          </div>
        </main>
        <div className={this.state.theme?"bottom-container-dark":"bottom-container"} id="bottomContainer"></div>
      </div>
    );
  }
}

export default App;
